# A Web server with a simple API using Bottle
#
# This server maintains a local data structure (a Python set) of widget names
# An agent connecting to this API server can use the following HTTP methods:
# GET    - retrieve a list of all widgets in the set
# POST   - add a new widget to the set...use JSON Content-Type in this format: {'model': 'WIDGET_NAME'}
# PUT    - change the name of a widget
# DELETE - remove a widget from the set

from bottle import run, default_app, route, get, post, put, delete, response, request
import json, re
import sys

# the widget "database" is emulated with a Python set data structure
_widget_models = set()

# limit the widget names to alphanumeric characters with max length of 64
namepattern = re.compile(r'^[a-zA-Z\d]{1,64}$')

# POST http://HOSTNAME/widget_models
@post('/widget_models')  
def creation_handler():
    # creates a new widget entry in the data set
    # this is expecting Content-Type of json in the following format: {'model': 'WIDGET_NAME'}
    # where "model" is a literal keyname and WIDGET_NAME is the user-supplied widget name
    print(request.json)
    try:
        # parse input data
        try: data = request.json
        except: raise ValueError

        if data is None: raise ValueError

        # extract and validate name
        try:
            if namepattern.match(data['model']) is None: raise ValueError
            name = data['model']
        except (TypeError, KeyError): raise ValueError

        # check for existence
        if name in _widget_models: raise KeyError

    except ValueError:
        # if bad request data, return 400 Bad Request
        response.status = 400
        return

    except KeyError:
        # if name already exists, return 409 Conflict
        response.status = 409
        return

    # add name
    _widget_models.add(name)

    # return 200 Success
    response.headers['Content-Type'] = 'application/json'
    return json.dumps({'model': name})

# GET http://HOSTNAME/widget_models
@get('/widget_models')
def listing_handler():
    # returns a json list of widgets that exist in the set
    response.headers['Content-Type'] = 'application/json'
    response.headers['Cache-Control'] = 'no-cache'
    return json.dumps({'widget_models': list(_widget_models)})

# PUT http://HOSTNAME/widget_models/EXISTING_WIDGET_NAME
@put('/widget_models/<oldname>')
def update_handler(oldname):
    # updates the name of an existing widget with a newly provided name
    try:
        # parse input data
        try: data = request.json
        except: raise ValueError

        # extract and validate new name
        try:
            if namepattern.match(data['model']) is None: raise ValueError
            newname = data['model']
        except (TypeError, KeyError): raise ValueError

        # check if updated name exists
        if oldname not in _widget_models: raise KeyError(404)

        # check if new name exists
        if newname in _widget_models: raise KeyError(409)

    except ValueError:
        response.status = 400
        return
    except KeyError as e:
        response.status = e.args[0]
        return

    # remove old name and add new name
    _widget_models.remove(oldname)
    _widget_models.add(newname)

    # return 200 Success
    response.headers['Content-Type'] = 'application/json'
    return json.dumps({'model': newname})

# DELETE http://HOSTNAME/widget_models/EXISTING_WIDGET_NAME
@delete('/widget_models/<name>')
def delete_handler(name):
    # deletes an entry from the set
    try:
        # Check if name exists
        if (name not in _widget_models) and (name != 'all'): raise KeyError
    except KeyError:
        response.status = 404
        return

    # Remove name
    if name == 'all':
        _widget_models.clear()
    else:
        _widget_models.remove(name)
    return

if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('must specify listening port as argument...')
    else:
        try: port = int(sys.argv[1])
        except: port = 80
        print('widget API server listening on port {}'.format(port))
        run(host='0.0.0.0', port=port, debug=True)
