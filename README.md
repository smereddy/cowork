
# Cowork Project for Acertara Software Team

* The cowork repository contains a Web server application written in Python using Bottle
* The Web server exposes a very simple API to interact with a list ("database") of "widgets" (simple strings).
* The intent is for interviewees to perform the following activities:
    * Clone this repository and retrieve the server code to review its functionality
    * Write their own client to interface with the API and demonstrate it
    * Commit/push their client code to this repository
* After interviews are complete, any client code is deleted from the repository
    * This ensures that the next interviewee does not pull that client code during their interview
    * Those clients that are pushed are still retrievable by cloning or checking out a specific commit ID, if desired
